import React from "react"

export interface ILinkProps {
  link: string;
  text: string;
}
const LinkComponent: React.FC<ILinkProps> = ( { link, text }) => (
  <a
    className="App-link"
    href={link}
    target="_blank"
    rel="noopener noreferrer"
  >
    {text}
  </a>
)

export { LinkComponent }