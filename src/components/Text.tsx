import React from "react"

const TextComponent: React.FC = ({ children }) => (
  <p>
    {children}
  </p>
)

export { TextComponent }