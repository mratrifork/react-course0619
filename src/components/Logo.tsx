import React from "react"

export interface ILogoProps {
  source: string;
  description: string;
}
const LogoComponent: React.FC<ILogoProps> = ({ source, description }) => (
  <img src={source} className="App-logo" alt={description} />
)

export { LogoComponent }