import React, { useState } from 'react';
import './App.css';
import { MovieList, IMovie } from 'pages/MovieList';
import { BrowserRouter, Switch, Route, RouteComponentProps, Redirect, NavLink} from "react-router-dom"
import data from "data/db.json"

export type OnMovieSelectedCallback = (movie: IMovie) => void;

const Header: React.FC = ({ children }) => (
  <header className="app__header">
    { children }
  </header>
)
const Content: React.FC = ({ children }) => <section className="app__content">{children}</section>

interface IMovieDetailParams {
  movieId: string;
}

interface IMovieDetailProps {
  onAddToFavoriteClicked: (movie: IMovie) => void;
}

const MovieDetail: React.FC<RouteComponentProps<IMovieDetailParams> & IMovieDetailProps> = ({ match, onAddToFavoriteClicked }) => {
  
  const movie = data.movies.find(movie => movie.id === Number(match.params.movieId))

  if (movie) {
    const onButtonClick = () => {
      onAddToFavoriteClicked(movie)
    }

    return (
      <div className="MovieDetail">
        <div><b>Title:</b> {movie.title}</div>
        <div><b>Plot:</b> {movie.plot}</div>
        <div><b>Year:</b> {movie.year}</div>
        <div><b>Director:</b> {movie.director}</div>
        <div><b>Actors:</b> {movie.actors}</div>
        <input type="button" value="Add to favorites" onClick={onButtonClick} />
      </div>
    )
  }

  return <div>No movie found with that id</div>
}

const App: React.FC = () => {
  const [favorites, setFavorites] = useState<IMovie[]>([])

  const onAddFavoriteClicked = (movie: IMovie) => {
    setFavorites([...favorites, movie])
  }

  return (
    <BrowserRouter>
      <div className="app">
        <Header>
          <NavLink className="nav_link" to="/movies">Movies</NavLink>
          <NavLink className="nav_link" to="/favorites">Favorites</NavLink>
        </Header>
        <Content>
          <Switch>
            <Route exact path="/movies/:movieId" render={routeComponentProps => <MovieDetail {...routeComponentProps} onAddToFavoriteClicked={onAddFavoriteClicked}/>} />
            <Route exact path="/movies" render={routeComponentProps => <MovieList {...routeComponentProps} movies={data.movies} />} />
            <Route exact path="/favorites" render={routeComponentProps => <MovieList {...routeComponentProps} movies={favorites} />} />
          </Switch>
        </Content>
      </div>
    </BrowserRouter>
  );
}

export { App };
