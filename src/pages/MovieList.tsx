import React from "react"
import "./MovieList.css"
import { OnMovieSelectedCallback } from "App";
import { RouteComponentProps } from "react-router-dom";

export interface IMovie {
  id: number;
  title: string;
  year: string;
  runtime: string;
  genres: string[];
  director: string;
  actors: string;
  plot: string;
  posterUrl: string;
}

interface IMovieListProps {
  movies: IMovie[];
}

export const MovieList: React.FC<RouteComponentProps & IMovieListProps> = ({ history, movies }) => {

  const onMovieSelected = (movie: IMovie) => {
    history.push(`/movies/${movie.id}`)
  }
    
  return (
    <ul className="MovieList">
      {
        movies.map(movie => <MovieListItem key={movie.id} movie={movie} onClick={onMovieSelected} />)
      }
    </ul>
  )
}
  
interface IMovieListItemProps {
  movie: IMovie;
  onClick: OnMovieSelectedCallback;
}

export const MovieListItem: React.FC<IMovieListItemProps> = ({ movie, onClick }) => {
  const { title, posterUrl} = movie

  const onMovieClicked = () => {
    onClick(movie);
  }

  return (
    <li className="MovieList__MovieListItem" onClick={onMovieClicked}>
      <h2>{title}</h2>
      <img src={posterUrl} alt={title} />
    </li>
  )
}




